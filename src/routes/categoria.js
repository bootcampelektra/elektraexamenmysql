const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/categoria/todos", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM Categoria;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});


router.get("/api/soloCategoria/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM Categoria WHERE idCategoria = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/categoria/guardar/:id",  (request, response) => {
  const idAdmin = request.params.id;
  const nombre = request.body.nombre;

  var cantidad = 0;
  

  
  

  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }


 async function validar(){ 
  return new Promise((resolve, reject) => {
    mysqlConnection.query(
      "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
      [idAdmin],
      (err, rows, fields) =>  {
        if (!err) {
           resolve(rows[0].cantidad);
        } else {
          response.json(err);
        }
      }
    );
  })
  
  }

  async function comprobar() {
   const cantidad = await validar();
    
  
  if (cantidad > 0){
  
    mysqlConnection.query(
     "INSERT INTO Categoria (nombre) VALUES (?);",
     [nombre ],
     (err, rows, fields) => {
       if (!err) {
         response.json({
          msg: "Categoria agregada correctamente"
         })
       } else {
         response.json(err);
       }
     }
   );
   }else{
     return response.status(401).json({
       msg: "Empleado no autorizado"
     }); 
   }
   
  }

  comprobar();
  

 
  
});

//PUT

router.put("/api/categoria/modificar/:id/:idAdmin", (request, response) => {
  const id = request.params.id;
  const idAdmin = request.params.idAdmin;
  const nombre = request.body.nombre;
  let cantidad = 0;
  

  
  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }


  async function validar(){ 
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
        [idAdmin],
        (err, rows, fields) =>  {
          if (!err) {
             resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    })
    
    }

  
    async function comprobar() {
      const cantidad = await validar();
       
     
     if (cantidad > 0){

  mysqlConnection.query(
    "UPDATE Categoria SET nombre = ?  WHERE idCategoria = ?;",
    [nombre, id ],
    (err, rows, fields) => {
      if (!err) {
          response.json({
            msg: "Categoria modificada"
          })

      } else {
        response.json(err);
      }
    }
  );

  } else{
    return response.status(401).json({
      msg: "Empleado no autorizado"
    }); 
  }


}

 comprobar();

});


//DELETE

router.delete("/api/categoria/eliminar/:id/:idAdmin", (request, response) => {
  const id = request.params.id;
  const idAdmin = request.params.idAdmin;
  let cantidad = 0;


  async function validar(){ 
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
        [idAdmin],
        (err, rows, fields) =>  {
          if (!err) {
             resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    })
    
    }


    async function comprobar() {
      const cantidad = await validar();
       
     
     if (cantidad > 0){


  mysqlConnection.query(
    "DELETE FROM Categoria WHERE idCategoria = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Categoria Eliminada"
        })
      } else {
        response.json(err);
      }
    }
  ); 
     }else{
      return response.status(401).json({
        msg: "Empleado no autorizado"
      }); 
    }
   
  }
  comprobar();
});

module.exports = router;
