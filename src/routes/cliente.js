const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/cliente/todos", (resquest, response) => {
  mysqlConnection.query("SELECT b.idCliente, a.nombre, a.apellido, a.telefono,  a.edad, a.correo, b.numCliente  FROM Cliente as b  INNER JOIN Persona as a  ON b.idCliente = a.idPersona;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});


router.get("/api/cliente/solo/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idCliente, a.nombre, a.apellido, a.telefono,  a.edad, a.correo, b.numCliente  FROM Cliente as b  INNER JOIN Persona as a  ON b.idCliente = a.idPersona WHERE b.idCliente = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/cliente/guardar/:id",  (request, response) => {
  const idAdmin = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const numCliente = request.body.numCliente;
  var cantidad = 0;
  

  
  

  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }

  if (!apellido) {
    return response.status(401).json({
      msg: "No puede ir el campo apellido vacío"
    });

  }


  if (!isNaN(edad) === false || !edad) {

    return response.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(telefono) === false || !telefono ) {

    return response.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
    return response.status(401).json({
      msg: "Proporcione un correo correcto"
    });
  }

 
  if (!numCliente) {
    return response.status(401).json({
      msg: "No puede ir el campo número empleado vacío"
    });

  }
 

 async function validar(){ 
  return new Promise((resolve, reject) => {
    mysqlConnection.query(
      "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
      [idAdmin],
      (err, rows, fields) =>  {
        if (!err) {
           resolve(rows[0].cantidad);
        } else {
          response.json(err);
        }
      }
    );
  })
  
  }

  async function comprobar() {
   const cantidad = await validar();
    
  
  if (cantidad > 0){
  
    mysqlConnection.query(
     "INSERT INTO Persona (nombre, apellido ,edad, telefono, correo) VALUES (?,?,?,?,?);",
     [nombre, apellido, edad, telefono, correo ],
     (err, rows, fields) => {
       if (!err) {
         const id = rows.insertId;
       
 
         mysqlConnection.query(
         "INSERT INTO Cliente (idCliente, numCliente) VALUES (?,?);",
         [id ,numCliente],
         (err, rows, fields) => {
           if (!err) {
             response.json({
               msg: "Cliente agregado con éxito"
             });
           } else {
             response.json(err);
           }
         }
       );
       } else {
         response.json(err);
       }
     }
   );
   }else{
     return response.status(401).json({
       msg: "Empleado no autorizado"
     }); 
   }
   
  }

  comprobar();
  

 
  
});

//PUT

router.put("/api/cliente/modificar/:id/:idAdmin", (request, response) => {
  const id = request.params.id;
  const idAdmin = request.params.idAdmin;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const numCliente = request.body.numCliente;
  let cantidad = 0;
  

  
  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }

  if (!apellido) {
    return response.status(401).json({
      msg: "No puede ir el campo apellido vacío"
    });

  }


  if (!isNaN(edad) === false || !edad) {

    return response.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(telefono) === false || !telefono ) {

    return response.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
    return response.status(401).json({
      msg: "Proporcione un correo correcto"
    });
  }

 
  if (!numCliente) {
    return response.status(401).json({
      msg: "No puede ir el campo número empleado vacío"
    });

  }

  async function validar(){ 
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
        [idAdmin],
        (err, rows, fields) =>  {
          if (!err) {
             resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    })
    
    }

  
    async function comprobar() {
      const cantidad = await validar();
       
     
     if (cantidad > 0){

  mysqlConnection.query(
    "UPDATE Persona SET nombre = ?, apellido = ? ,edad = ?, telefono = ?, correo = ? WHERE idPersona = ?;",
    [nombre, apellido, edad, telefono, correo, id ],
    (err, rows, fields) => {
      if (!err) {
      

        mysqlConnection.query(
        "UPDATE Cliente SET  numCliente = ? WHERE idCliente = ?;",
        [numCliente, id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Cliente modificado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );

  } else{
    return response.status(401).json({
      msg: "Empleado no autorizado"
    }); 
  }


}

 comprobar();

});


//DELETE

router.delete("/api/cliente/eliminar/:id/:idAdmin", (request, response) => {
  const id = request.params.id;
  const idAdmin = request.params.idAdmin;
  let cantidad = 0;


  async function validar(){ 
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
        [idAdmin],
        (err, rows, fields) =>  {
          if (!err) {
             resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    })
    
    }


    async function comprobar() {
      const cantidad = await validar();
       
     
     if (cantidad > 0){


  mysqlConnection.query(
    "DELETE FROM Persona WHERE idPersona = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Cliente Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
     }else{
      return response.status(401).json({
        msg: "Empleado no autorizado"
      }); 
    }
   
  }
  comprobar();
});

module.exports = router;
