const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/administrador/todos", (resquest, response) => {
  mysqlConnection.query("SELECT b.idAdministrador, a.nombre, a.apellido, a.telefono,  a.edad, a.correo, b.numEmpleado  FROM Administrador as b  INNER JOIN Persona as a  ON b.idAdministrador = a.idPersona;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});


router.get("/api/administrador/solo/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idAdministrador, a.nombre, a.apellido, a.telefono,  a.edad, a.correo, b.numEmpleado  FROM Administrador as b  INNER JOIN Persona as a  ON b.idAdministrador = a.idPersona WHERE b.idAdministrador = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/administrador/guardar", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const numEmpleado = request.body.numEmpleado;
  
  

  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }

  if (!apellido) {
    return response.status(401).json({
      msg: "No puede ir el campo apellido vacío"
    });

  }


  if (!isNaN(edad) === false || !edad) {

    return response.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(telefono) === false || !telefono ) {

    return response.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
    return response.status(401).json({
      msg: "Proporcione un correo correcto"
    });
  }

 
  if (!numEmpleado) {
    return response.status(401).json({
      msg: "No puede ir el campo número empleado vacío"
    });

  }
 

  mysqlConnection.query(
    "INSERT INTO Persona (nombre, apellido ,edad, telefono, correo) VALUES (?,?,?,?,?);",
    [nombre, apellido, edad, telefono, correo ],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
      

        mysqlConnection.query(
        "INSERT INTO Administrador (idAdministrador, numEmpleado) VALUES (?,?);",
        [id ,numEmpleado],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Administrador agregado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/administrador/modificar/:id", (request, response) => {
  const id = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const numEmpleado = request.body.numEmpleado;
  

  
  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }

  if (!apellido) {
    return response.status(401).json({
      msg: "No puede ir el campo apellido vacío"
    });

  }


  if (!isNaN(edad) === false || !edad) {

    return response.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(telefono) === false || !telefono ) {

    return response.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
    return response.status(401).json({
      msg: "Proporcione un correo correcto"
    });
  }

 
  if (!numEmpleado) {
    return response.status(401).json({
      msg: "No puede ir el campo número empleado vacío"
    });

  }

  mysqlConnection.query(
    "UPDATE Persona SET nombre = ?, apellido = ? ,edad = ?, telefono = ?, correo = ? WHERE idPersona = ?;",
    [nombre, apellido, edad, telefono, correo, id ],
    (err, rows, fields) => {
      if (!err) {
      

        mysqlConnection.query(
        "UPDATE Administrador SET  numEmpleado = ? WHERE idAdministrador = ?;",
        [numEmpleado, id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Administrador modificado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );



});


//DELETE

router.delete("/api/administrador/eliminar/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Persona WHERE idPersona = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Administrador Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
