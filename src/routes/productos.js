const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/producto/todos", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM Producto;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});


router.get("/api/soloProducto/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM Producto WHERE idProducto = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/producto/guardar/",  (request, response) => {
  const nombre = request.body.nombre;
  const stock = request.body.stock;
  const categoria = request.body.categoria;
  const precio  = request.body.precio;

  var cantidad = 0;
  

  
  

  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }
  
  if (!isNaN(stock) === false || !stock ) {

    return response.status(401).json({
      msg: "No puede ir el dato stock vacío y debe ser númerico"
    });
  }

  if (!isNaN(categoria) === false || !categoria ) {

    return response.status(401).json({
      msg: "No puede ir el dato categoria vacío y debe ser númerico, además que debe de existir"
    });
  }

  if (!isNaN(precio) === false || !precio ) {

    return response.status(401).json({
      msg: "No puede ir el dato precio vacío y debe ser númerico"
    });
  }

 

  
    mysqlConnection.query(
     "INSERT INTO Producto (nombre,stock,precio,categoria) VALUES (?,?,?,?);",
     [nombre, stock, precio,  categoria],
     (err, rows, fields) => {
       if (!err) {
         response.json({
          msg: "Producto agregado correctamente"
         })
       } else {
         response.json(err);
       }
     }
   );
   
  
});

//PUT

router.put("/api/producto/modificar/:id/:idAdmin", (request, response) => {
  const id = request.params.id;
  const idAdmin = request.params.idAdmin;
  const nombre = request.body.nombre;
  const stock = request.body.stock;
  const categoria = request.body.categoria;
  const precio  = request.body.precio;

  var cantidad = 0;
  

  
  

  // Validaciones
 
  if (!nombre) {
    return response.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }
  
  if (!isNaN(stock) === false || !stock ) {

    return response.status(401).json({
      msg: "No puede ir el dato stock vacío y debe ser númerico"
    });
  }

  if (!isNaN(categoria) === false || !categoria ) {

    return response.status(401).json({
      msg: "No puede ir el dato categoria vacío y debe ser númerico, además que debe de existir"
    });
  }

  if (!isNaN(precio) === false || !precio ) {

    return response.status(401).json({
      msg: "No puede ir el dato precio vacío y debe ser númerico"
    });
  }



  async function validar(){ 
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
        [idAdmin],
        (err, rows, fields) =>  {
          if (!err) {
             resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    })
    
    }

  
    async function comprobar() {
      const cantidad = await validar();
       
     
     if (cantidad > 0){

  mysqlConnection.query(
    "UPDATE Producto SET nombre = ?, stock = ?, categoria = ?, precio = ?  WHERE idProducto = ?;",
    [nombre, stock, categoria, precio,  id ],
    (err, rows, fields) => {
      if (!err) {
          response.json({
            msg: "Producto modificado"
          })

      } else {
        response.json(err);
      }
    }
  );

  } else{
    return response.status(401).json({
      msg: "Empleado no autorizado"
    }); 
  }


}

 comprobar();

});


//DELETE

router.delete("/api/producto/eliminar/:id/:idAdmin", (request, response) => {
  const id = request.params.id;
  const idAdmin = request.params.idAdmin;
  let cantidad = 0;


  async function validar(){ 
    return new Promise((resolve, reject) => {
      mysqlConnection.query(
        "SELECT COUNT(*) as cantidad FROM Administrador WHERE numEmpleado = ?",
        [idAdmin],
        (err, rows, fields) =>  {
          if (!err) {
             resolve(rows[0].cantidad);
          } else {
            response.json(err);
          }
        }
      );
    })
    
    }


    async function comprobar() {
      const cantidad = await validar();
       
     
     if (cantidad > 0){


  mysqlConnection.query(
    "DELETE FROM Producto WHERE idProducto = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Producto Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
     }else{
      return response.status(401).json({
        msg: "Empleado no autorizado"
      }); 
    }
   
  }
  comprobar();
});

module.exports = router;
