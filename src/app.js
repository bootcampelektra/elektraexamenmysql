const express = require('express');

const app = express();

//Configuration Server
app.set('port', process.env.PORT || 3000);


//Middlewares
app.use(express.json());


//Routes
app.use(require('./routes/cliente'));
app.use(require('./routes/productos'));
app.use(require('./routes/administrador'));
app.use(require('./routes/categoria'));



app.listen(app.get('port'), () =>{
    console.log('Servidor activo en puerto '+ app.get('port'));
})